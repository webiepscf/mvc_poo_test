<?php
/*
  ./app/routeur.php
 */

 if (isset($_GET['posts'])):
   include '../app/routeurs/postsRouteur.php';

 elseif (isset($_GET['categories'])):
   include '../app/routeurs/categoriesRouteur.php';

 else:

   /*
   ROUTE 1:
   PATTERN: /
   CTRL: ArticlesControleur
   ACTION: indexAction
    */
    $ctrl = new \App\Controleurs\PostsControleur();
    $ctrl->indexAction();
  endif;




/*


Routeur::get('/', 'postControleur@indexAction')->name('accueil');
Routeur::get('/posts/:id/:slug', 'postControleur@indexAction')->name('posts.show');

posts/56/kjhkjh

<a href="<?php echo Routeur::getRoute('posts.show') ?>">khkjh</a>



*/
