<?php
/*
./app/modeles/PostsGestionnaire.php
 */

namespace App\Modeles;
use \Noyau\Classes\App;

class PostsGestionnaire extends \Noyau\Classes\GestionnaireGenerique {

  public function __construct(){
    $this->_table = 'posts';
    $this->_modele = '\App\Modeles\Post';
  }

public function findAllByCategorie(int $id){
  $sql="SELECT *
        FROM posts
        JOIN posts_has_categories ON post=posts.id
        WHERE categorie = :id;";
  $rs= App::getConnexion()->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();
  return $this->convertPDOStatementToArrayObj($rs);
}

}
