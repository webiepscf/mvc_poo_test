<?php
  use \Noyau\Classes\App;
  use \Noyau\Classes\Template;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>MVC POO - <?php echo Template::getZone('title'); ?></title>
  <base href="<?php echo App::getRoot(); ?>" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
</head>
<body class="container">
  <h1><a href="<?php echo App::getRoot(); ?>">MVC POO</a></h1>
  <main class="row">
    <div class="col m8">

      <?php echo Template::getZone('content1'); ?>

    </div>
    <aside class="col m4">
      <?php
        $ctrl = new \App\Controleurs\CategoriesControleur();
        $ctrl->indexAction([
          'orderBy' => 'titre'
        ]);
       ?>
       <div class="">
         <?php
           $ctrl = new \App\Controleurs\PostsControleur();
           $ctrl->indexAction([
             'orderBy'   => 'datePublication',
             'orderSens' => 'DESC',
             'limit'     => 2
           ], 'lastest');
          ?>
       </div>
    </aside>
  </main>

</body>
</html>
