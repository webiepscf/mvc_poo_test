<?php
/*
./app/vues/posts/index.php
Variables disponibles :
    - $posts ARRAY(Post)
 */
use \Noyau\Classes\Template;
?>
<?php
 /* ---------------------------------------------------
      ZONE TITLE
    ---------------------------------------------------
  */ ?>
<?php Template::startZone('title'); ?>
  Liste des posts
<?php Template::stopZone(); ?>

<?php
 /* ---------------------------------------------------
      ZONE CONTENT1
    ---------------------------------------------------
  */ ?>
<?php Template::startZone('content1'); ?>

  <h2>Ceci est la page index des posts</h2>
  <?php include '../app/vues/posts/liste.php'; ?>

<?php Template::stopZone(); ?>
