<?php

namespace Noyau\Classes;

abstract class Template {

  private static $_zone;
  private static $_zones;

  public static function getZone(string $zone){
    return SELF::$_zones[$zone];
  }

  public static function setZones(string $zones){
    if ($zones):
      foreach($zones as $zone):
        SELF::$_zones[$zone] = '';
      endforeach;
    endif;

  }

  public static function startZone(string $zone){
    ob_start();
    SELF::$_zone = $zone;
  }

  public static function stopZone(){
    SELF::$_zones[SELF::$_zone] = ob_get_clean();
  }

}
